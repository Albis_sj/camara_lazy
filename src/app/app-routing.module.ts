import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


const routes: Routes = [

  {path: '',
  loadChildren: () => import('./components/components.module').then(x => x.ComponentsModule)},

  { path: 'admin',
  loadChildren: () => import('./admin/admin.module').then(x => x.AdminModule)
},

  { path: 'perfil',
  loadChildren: () => import('./perfil/perfil.module').then(x => x.PerfilModule)
},
{ path: 'auth',
  loadChildren: () => import('./auth/auth.module').then(x => x.AuthModule)
},

  {path: '**', pathMatch: 'full', redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
