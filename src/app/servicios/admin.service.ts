import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  // Comunicados
  apiUrlCom = 'http://localhost:3000/addcomunicado';

  // Eventos
  apiUrlEven = 'http://localhost:3000/addEvent';

  constructor(private _http: HttpClient,) { }

  
  // ----------------------------------------------------------------
  // COMUNICADOS

  //get all data
  getAllDataCom(): Observable<any>{
    return this._http.get(`${this.apiUrlCom}`);
  }
  //create data
  createDataCom(data:any,):Observable<any>{
    console.log(data, 'create Api Comunicados =>');
    return this._http.post(`${this.apiUrlCom}`, data);
  }
  //delete data
  deleteDataCom(id:any):Observable<any>{
    let ids = id;
    console.log(id, ids);
    return this._http.delete(`${this.apiUrlCom}/${ids}`);
  }
  //update data
  updateDataCom(data:any, id: any):Observable<any>{
    let ids = id;
    console.log('ids', id);
    return this._http.put(`${this.apiUrlCom}/${ids}`, data);
  }
  // COMUNICADOS
  // ----------------------------------------------------------------
  
  

  // ----------------------------------------------------------------
  // EVENTOS

  
  //get all data
  getAllDataEvent(): Observable<any>{
    console.log('evento');
    
    return this._http.get(`${this.apiUrlEven}`);
  }

  //create data
  createDataEvent(data:any,):Observable<any>{
    console.log(data, 'create Api Vendedor =>');
    return this._http.post(`${this.apiUrlEven}`, data);
  }

  //delete data
  deleteDataEvent(id:any):Observable<any>{
    let ids = id;
    console.log(id, ids);
    return this._http.delete(`${this.apiUrlEven}/${ids}`);
  }

  //update data
  updateDataEvent(data:any, id: any):Observable<any>{
    let ids = id;
    console.log('ids', id);
    return this._http.put(`${this.apiUrlEven}/${ids}`, data);
  }




}
