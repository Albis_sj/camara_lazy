import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  
  user!: VendedorI;
  ID_user!: string;

  constructor(private _http: HttpClient,
    private router: Router) { }

    apiUrl = 'http://localhost:3000/vendedor';
  Vendedor!: VendedorLoginI;
  
  
  //get all data
  getAllData(): Observable<any>{
    return this._http.get(`${this.apiUrl}`);
  }

  //get all data
  getOneData(id: number): Observable<any>{
    console.log(id, 0);
    return this._http.get(`${this.apiUrl}/${id}`);
  }

  //create data
  createData(data:any,):Observable<any>{
    console.log(data, 'create Api Vendedor =>');

    return this._http.post(`${this.apiUrl}`, data);
  }

  //update data
  updateData(data:any, id: any):Observable<any>{
    let ids = id;
    // console.log('SERVICE',data, id, 'SERVICE');
    // console.log(`${this.apiUrl}/${ids}`);
    
    
    return this._http.put(`${this.apiUrl}/${ids}`, data);
  }


  //LOGIN DATOS
  loginVendedor(user: VendedorI, id:number){
    this.ID_user = id.toString()
    this.user = user;
    sessionStorage.setItem('ID', this.ID_user);
    let nombre = sessionStorage.getItem('ID');
    
    this.router.navigate([ '/perfil' ])
    return  nombre;
  }

}


export interface VendedorI {
  nombre: string,
  id_vendedor: number,
  categoria: string,
  correo1: string,
  contrasenia: string,
  telefono1: string,
  telefono2: string,
  celular1: number,
  celular2: string,
  correo2: number,
  direccion: string,
  pagina: string
};

export interface VendedorLoginI {
  correo1: string,
  contrasenia: string
}