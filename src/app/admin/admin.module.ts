import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { InicioAdminComponent } from './inicio-admin/inicio-admin.component';
import { AddEventoComponent } from './add-evento/add-evento.component';
import { AddComunicadoComponent } from './add-comunicado/add-comunicado.component';
import { AddAsociadoComponent } from './add-asociado/add-asociado.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    InicioAdminComponent,
    AddEventoComponent,
    AddComunicadoComponent,
    AddAsociadoComponent
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    ReactiveFormsModule
  ]
})
export class AdminModule { }
