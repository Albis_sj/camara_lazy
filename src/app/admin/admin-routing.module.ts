import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddAsociadoComponent } from './add-asociado/add-asociado.component';
import { AddComunicadoComponent } from './add-comunicado/add-comunicado.component';
import { AddEventoComponent } from './add-evento/add-evento.component';
import { InicioAdminComponent } from './inicio-admin/inicio-admin.component';

const routes: Routes = [
  { path: '', component: InicioAdminComponent},
  { path: 'addComunicado', component: AddComunicadoComponent},
  { path: 'addEvento', component: AddEventoComponent},
  { path: 'addAsociado', component: AddAsociadoComponent},
  { path: '**', pathMatch: 'full', redirectTo: './'},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
