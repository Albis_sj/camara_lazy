import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LibroService } from 'src/app/servicios/libro.service';

@Component({
  selector: 'app-libros',
  templateUrl: './libros.component.html',
  styleUrls: ['./libros.component.css']
})
export class LibrosComponent implements OnInit {

  formato:any;

  public page: number = 0;
  public number: number = 1;
  pageActual:number = 1;

  propiedades: any = {
    error: true
  }

  // BUSCADOR
  termino!: string;
  libros: any[] = [];
  mostrarBuscador: boolean = true
  mostrarFiltro: boolean = false
  // BUSCADOR

  constructor(private libroSVC: LibroService,
              private router: Router,
              private activatedRoute: ActivatedRoute) {
    this.Categoria();
    this.Libros();
    this.Formato();

    this.activatedRoute.params.subscribe(params => { //subscribe obtiene una variable
      console.log(params['termino'], 'Init del termino');
      this.termino = params['termino']; //se lo guarda en una variables lo que hace log, y esa variable 
      console.log(this.termino, 'termino = params');

      //NO PASA DE AQUI
     if(this.termino !== undefined){
      this.libroSVC.getBuscar(this.termino.toLowerCase()).subscribe((res)=>{
        this.libros = res.data
        console.log(this.libros);
        
      })
     } else {
      console.log('undefined,, ');
      this.mostrarBuscador = false

     }

      console.log(this.libros, 'lista de libros');
    })

  }

  ngOnInit(): void {
  }

  public readCategoria: any;
  public readData: any;
  public readFiltro: any;
  readFormato: any;
  Categoria(){
    console.log('DAta CAtegoria');
    
    this.libroSVC.getAllDataCate().subscribe((res)=>{
      // console.log(res, "res==> categoria");
      this.readCategoria = res.data;
      // console.log('read Categoria 000',this.readCategoria, 'read CAtegoria 000');
    });
  }

  getLibroCategoria(id: number){
    console.log(id, '000');
    
    this.libroSVC.getLibroCategoria(id).subscribe((res)=>{
      this.readFiltro = res.data;
      this.mostrarFiltro = true;
      console.log('filtro Categoria 000',this.readFiltro, 'filtro CAtegoria 000');
    });
  }
  
  getLibroFormato(id: string){
    console.log(id, '000');
    
    this.libroSVC.getLibroFormato(id).subscribe((res)=>{
      this.readFiltro = res.data;
      this.mostrarFiltro = true;
      console.log('filtro Categoria 000',this.readFiltro, 'filtro CAtegoria 000');
    });
  }


  Formato(){
    this.formato = this.libroSVC.getFormato()
  }

  Libros(){
    this.libroSVC.getAllData().subscribe((res)=>{
      console.log(res, "res==> libros");
      this.readData = res.data;
    });
  }

  buscarLibro(termino: string): void{
    this.router.navigate(['/buscar', termino]); //al apretar enter o el boton, aqui ira al componente de buscar, y el termino como pusimos en routes, y entra a la clase buscadorComponente
  }




  VerLibro(id:number){
    this.router.navigate(['/Un-libro', id]);
  }
  
  //

}

