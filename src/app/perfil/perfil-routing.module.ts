import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CrearLibroComponent } from './crear-libro/crear-libro.component';
import { PerfilComponent } from './perfil.component';
import { UsuarioComponent } from './usuario/usuario.component';

const routes: Routes = [
  {
    path: '',
    component: PerfilComponent,
    children: [
      { path: '', component: UsuarioComponent},
      { path: 'user/:id', component: UsuarioComponent},
      { path: 'crear-libro/:id', component: CrearLibroComponent},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PerfilRoutingModule { }
