import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LibroI, LibroService } from 'src/app/servicios/libro.service';
import { UsuarioService, VendedorI } from 'src/app/servicios/usuario.service';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})
export class UsuarioComponent implements OnInit {
  
  idioma: string[] = ['Español', 'Inglés'];
  estado: string[] = ['Nuevo', 'Muy bueno', 'Bueno', 'Sin especificar'];
  edad: string[] = ['Niños (0-5 años)', 'Niños (5-10)', 'Jovenes (12-17)', 'Adolescentes (17-18)', '(20- 28)', 'Mayores', 'Todo público'];
  formato: string[] = ['Libro de bolsillo', 'Libro en rústica', 'Tapa Dura', 'Anillado', 'Encuadernado con grapas', 'Encuadernado en cuero', 'Encuadernado flexi', 'Folleto, panfleto', 'Libro de cartón', 'Libro de goma espuma', 'Libro de tela', 'Libro electrónico', 'Con mapa plegable', 'Sin especificar'];
  formulario!: FormGroup;
  formPerfil!: FormGroup;
  Form = {};
  tipo_archivo: any;
  
  // IMAGEN
  url = 'https://i.pinimg.com/736x/b5/49/41/b5494197b2d462c940f88988b203d290.jpg';
  @ViewChild('fileInput', { static: false }) fileInput!: ElementRef
  apiUrlImg = 'http://localhost:3000/addLibroImg';

  enviado: string = 'Registro finalizado con éxito';
  mensaje!: string;

  mostrar = false;
  userName!: number;
  tabla: boolean = true;
  ListaConFiltro:LibroI[] = [];
  ReadOnly : boolean = true;
  file!: File;
  
  propiedades: any = {
    error: false
  }
    
  Snack: any = {
    error: false
  }
  

  
  constructor(private _librosSVC: LibroService,
    private _vendedorSVC :UsuarioService,
    private fb: FormBuilder,
    private router: Router,
    private http: HttpClient) { 

      this.crearFormulario();
      this.getCategoria();
      
      let nombre = sessionStorage.getItem('ID')
      if (nombre === null){
        console.log('null');
        
        this.userName = 0
      } else {
        this.userName = parseInt(nombre)
        // console.log(this.userName, 'nombre');
        
        this.mostrar = true
        this.getPerfil()
        this.getAll();
        // console.log('getall');

        this.formPerfil = this.fb.group({
          nombre: [''],
          telefono: [''],
          celular: [''],
          direccion: [''],
          correo: [''],
          pagina: [''],
        })

        setTimeout(() => {
          console.log('2000');
          // console.log(this.readPerfil);
          this.formPerfil.patchValue({
            nombre : this.readPerfil.nombre,
            telefono: this.readPerfil.telefono1,
            celular : this.readPerfil.celular1,
            direccion: this.readPerfil.direccion,
            correo : this.readPerfil.correo1,
            pagina : this.readPerfil.pagina,
          });
  
        }, 1000);

      }
    }

     
  // VALIDACIONES
  get precioNoValido(){
    return this.formulario.get('precio')?.invalid && this.formulario.get('precio')?.touched
  }
  get id_categoriaNoValido(){
    return this.formulario.get('id_categoria')?.invalid && this.formulario.get('id_categoria')?.touched
  }
  get idiomaNoValido(){
    return this.formulario.get('idioma')?.invalid && this.formulario.get('idioma')?.touched
  }
  get autor_nombreNoValido(){
    return this.formulario.get('autor_nombre')?.invalid && this.formulario.get('autor_nombre')?.touched
  }
  get imagenNoValido(){
    return this.formulario.get('imagen')?.invalid && this.formulario.get('imagen')?.touched
  }
  get tituloNoValido(){
    return this.formulario.get('titulo')?.invalid && this.formulario.get('titulo')?.touched
  }
  get descripcionNoValido(){
    return this.formulario.get('descripcion')?.invalid && this.formulario.get('descripcion')?.touched
  }
  get formatoNoValido(){
    return this.formulario.get('formato')?.invalid && this.formulario.get('formato')?.touched
  }



    crearFormulario(){
      this.formulario = this.fb.group({
        
        precio: ['', Validators.required],
        // hora_publicacion:[''],
        estado: ['Sin Especificar'],
        id_categoria: ['', Validators.required],
        formato: [''],
        // ubicacion: [''],
        idioma: ['', Validators.required],
        autor_nombre: ['', [Validators.required, Validators.minLength(3)]],
        autor_apellido: ['', Validators.minLength(3)],
        anio_publicacion: [''],
        edad_publico: [''],
        numero_paginas: [''],
  
        titulo: ['', [Validators.required, Validators.minLength(3)]],
        subtitulo: ['', [Validators.minLength(3)]],
        descripcion: ['', [Validators.required, Validators.minLength(3)]],
  
        imagen: ['', Validators.required]
  
        // pago:  [''],
        // fecha_lim ite:  [''],
        // id_vendedor:  [''],
      })
     } 
  
    ngOnInit(): void {
    }

    readData: any;
    readCategoria: any;
    readPerfil: VendedorI = {
      nombre:'',
      id_vendedor: 0,
      categoria: '',
      correo1:'',
      correo2: 0,
      contrasenia: '',
      telefono1:'',
      telefono2: '',
      celular1: 0,
      celular2: '',
      direccion: '',
      pagina: '',
  
    };
  
    getAll(){
      this._librosSVC.getOneData(this.userName).subscribe((res) => {
        this.readData = res.data
        console.log(this.readData);
        

        if(this.readData === undefined){
          this.tabla= false
        }
      })
      // console.log(this._librosSVC.getOneData(this.userName));
    }
  
  guardar(){

console.log('guardar');

      let nombreImagen = this.formulario.value.imagen
          const imgName = nombreImagen.split('\\');
          const dividir = imgName[2].split('.');
          const extension = this.tipo_archivo.split('/');
  
      if(this.formulario.valid){
        console.log(this.formulario.value);
          let hora = new Date()
          this.Form = {
            precio: this.formulario.value.precio,
            hora_publicacion: hora,
            estado: this.formulario.value.estado,
            id_categoria: this.formulario.value.id_categoria,
            formato: this.formulario.value.formato,
            ubicacion: 'Cochabamba, borrar este campo',
            idioma: this.formulario.value.idioma,
            autor_nombre: this.formulario.value.autor_nombre,
            autor_apellido: this.formulario.value.autor_apellido,
            anio_publicacion: this.formulario.value.anio_publicacion,
            edad_publico: this.formulario.value.edad_publico,
            numero_paginas: this.formulario.value.numero_paginas,
  
            titulo: this.formulario.value.titulo,
            subtitulo: this.formulario.value.subtitulo,
            descripcion: this.formulario.value.descripcion,
  
            imagen1: `../../../../assets/img/libros/${dividir[0]}.${dividir[1]}`,
            tipo1: `.${extension[1]}`,
  
            pago: 'false',
            fecha_limite: '',
            id_vendedor: this.userName,
          }
  
          this.onFileUpload()
          
          this.mensaje = this.enviado;
          this.limpiarFormulario()
          setTimeout(() => {
            this.mensaje = '';
          }, 4000);
          
          this.Snack.error = true;
          setTimeout(() => {
            this.Snack.error =false;
          }, 4000);
          this.propiedades.error = false;
        this.createData(this.Form)

    }
  }


  UpdatePerfil(){
    console.log(this.formPerfil.value, this.readPerfil.id_vendedor);
    this.editarUsuario(this.formPerfil.value, this.readPerfil.id_vendedor)
    
  }
  
  limpiarFormulario(){
    this.formulario.reset()
  }
  
  getCategoria(){
    this._librosSVC.getAllDataCate().subscribe((res)=>{
      // console.log(res, "res==> CAtegorias");
      this.readCategoria = res.data;
    });
  }

  crearLibro(id:string){
    this.router.navigate([ `/perfil/crear-libro/${id}`])
  }  

  // On File Select
  onSelect(event: any) {
    this.file = event.target.files[0]
    console.log(this.file.type);
    const filew = event.target.files[0]
    this.tipo_archivo = filew.type

    if(event.target.files[0]){
      let reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (event: any) => {
        this.url = event.target.result

      }
    }
  }
  
  onFileUpload(){
    console.log('upload foto');

    console.log('FILE INOUT', this.fileInput.nativeElement.files[0], 'FILE INPUT');
    
    // this._librosSVC.onFileUpload(this.fileInput);
    const imageBlob = this.fileInput.nativeElement.files[0];
    const file = new FormData();
    console.log('prim', file, 'primero');

    file.set('file', imageBlob);

    this.http.post(`${this.apiUrlImg}`, file).subscribe(res => {
      // console.log(res);
      console.log('segundo', file, 'CUARTO');
    });
  }
  
  getPerfil(){
    this._vendedorSVC.getOneData(this.userName).subscribe((res)=>{
      this.readPerfil = res.data[0];
      console.log(this.readPerfil);
      
    });
  }
  
  //delete data
  deleteData(id: any){
    console.log(id, 'deleteid==>');
    this._librosSVC.deleteData(id).subscribe((res)=>{
      console.log(res, 'deleteRes ==>');
      this.getAll();
    });
  }
  // update Libro
  editarData(id:any){
    // console.log('Editar el libro');
    this.router.navigate([ `/perfil/crear-libro/${id}`])
  }
  // update Usuario
  editarUsuario(data:any, id:number){
    console.log(data);
    
    this._vendedorSVC.updateData(data, id).subscribe((res)=>{
      console.log('update');
      this.propiedades.error = false;
      this.getPerfil();
    });
  }
    
    //create data
    createData(data:any,){
      // console.log(data);
      this._librosSVC.createData(data).subscribe((res)=>{
        console.log(res, 'res create REgistro==>');
        this.getAll();
      });
    }
    
    editPerfil(){
      this.ReadOnly = false
    }
  
  
  }
  