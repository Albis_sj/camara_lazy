import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PerfilRoutingModule } from './perfil-routing.module';
import { UsuarioComponent } from './usuario/usuario.component';
import { PerfilComponent } from './perfil.component';
import { ReactiveFormsModule } from '@angular/forms';
import { CrearLibroComponent } from './crear-libro/crear-libro.component';


@NgModule({
  declarations: [
    UsuarioComponent,
    PerfilComponent,
    CrearLibroComponent
  ],
  imports: [
    CommonModule,
    PerfilRoutingModule,
    ReactiveFormsModule
  ],
  exports: [
    UsuarioComponent
  ]
})
export class PerfilModule { }
