import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  mostrar = false;
  userName!: number;
  alerta: string = 'alerta exitosa';
  propiedades: any = {
    visible: false
  }

  constructor(private router: Router) { 
    let nombre = sessionStorage.getItem('ID')
    if (nombre === null){
      this.userName = 0
    } else {
      this.userName = parseInt(nombre)
      this.mostrar = true
    }
  }

  ngOnInit(): void {
  }

  irAlPerfil(){
    console.log(this.userName, '000');
    this.router.navigate([ `perfil`])
  }
  
  buscarLibro(termino: string): void{
    console.log(termino);
    
    this.router.navigate(['/libros', termino]); //al apretar enter o el boton, aqui ira al componente de buscar, y el termino como pusimos en routes, y entra a la clase buscadorComponente
  }

}
